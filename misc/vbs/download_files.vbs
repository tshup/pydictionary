'' Downloads a list of files from a specific URL
'Dim uName As String
'Dim uPass As String
'Dim baseUrl As String
'Dim colDocs As Variant
Dim DataBin
'Dim xmlHttpDoc As Object
'Dim docName As Variant

Const adTypeBinary = 1
Const adSaveCreateOverWrite = 2
   
uName = "test"
uPass = "test"

docDir = "pydictionary.googlecode.com"

'rootUrl = "http://pydictionary.googlecode.com/"
prefixUrl = "http://pydictionary.googlecode.com/"


'the list of files to download
colDocs = Array("dict3.css", _
                "dict.psp", _
                "dict.js" _
                )

Set objFSO = CreateObject("Scripting.FileSystemObject")
If Not objFSO.FolderExists(docDir) Then objFSO.CreateFolder docDir

For Each docName In colDocs

    docUrl = prefixUrl & docName
    
    Set xmlHttpDoc = CreateObject("Microsoft.XMLHTTP")
    '' Set xmlHttpDoc = CreateObject("MSXML2.XMLHTTP")
    xmlHttpDoc.Open "GET", docUrl, False, uName, uPass
    xmlHttpDoc.send
    DataBin = xmlHttpDoc.responseBody
    
    Dim SendBinary
    Set SendBinary = CreateObject("ADODB.Stream")
    
    SendBinary.Type = adTypeBinary
    SendBinary.Open
    SendBinary.Write DataBin
    SendBinary.SaveToFile docDir & "\" & docName, adSaveCreateOverWrite
    
    Set SendBinary = Nothing
    Set xmlHttpDoc = Nothing

Next
