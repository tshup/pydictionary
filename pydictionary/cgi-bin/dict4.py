#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Description: 	Dictionary Lookup Form, CGI version.
        Uses sqlite3
Last update:	01-Mar-2009

FIXME!!!! fix highlight regex; identify and fix cause of some words not being highlighted in FTS
+FIXME: Unicode errors! DONE; need testing Review Unicode handling
+FIXME: SQL special symbols not escaped; DONE; needs testing
DONE: Store each dictionary in a separate SQLite file
TODO?: Verify existence of DB file
WON'T FIX: Case-insensitive search does not work for Unicode
FIXED: Unicode errors in dictionary names (via xmlcharrefreplace)
FIXME: Highlight in full-text search is not linkified
TODO: Add 'virtual keyboard for accented characters'
TODO: Remember current dictionary in cookie DONE
TODO: Support for images DONE

"""

import sys
import os
import cgi
import cgitb
import Cookie

cgitb.enable()
cgi
#import codecs

debug = False

dicts = dict(
collins=u"en: Collins Cobuild Dictionary, 5th edition",
bse=u'ru: Big Soviet Encyclopedia (CAPS)',
efremova=u'ru: Efremova',
soule=u"en: Soule's Dictionary of Synonyms",
wordspy=u"en: Wordspy Dictionary",
britannica=u"en: Britannica",
webster=u"en: Webster Collegiate",
longman=u"en: Longman (LDOCE)",
Apresyan=u"ru: Apresyan",
kiev09=u"09: Kiev09",
enua=u"ua: English-Ukrainian",
uaru=u"ua: Ukrainian-Russian",
dudenbig=u'de: Duden große Wörterbuch der deutschen Sprache'.encode('us-ascii','xmlcharrefreplace'),
#dudenbig=u"de: Duden",
mythru=u"ru: Mythological Encyclopedia",
#ldoce=u"en: Longman",
es_britannica=u"es: Spanish Britannica",
spanishvox=u'es: Spanish Esencial (Vox 31000)',
esen_lng=u"es: English-Spanish © 2000 Langenscheidt",
En_Es_Babylon=u"es: English-Spanish (Babylon)",
wordref=u"es: WordReference English-Spanish",
wordrefconj="es: WordReference Spanish Conjugations",
#esen_lng=u"es: English-Spanish 2000 Langenscheidt",
oed=u"en: Oxford English Dictionary",
es_larousse=u"es: Gran Diccionario de la Lengua Española LAROUSSE".encode('us-ascii','xmlcharrefreplace'),
esen_tony=u"es: Spanish-English (Tony Lozano)",
enes_tony=u"es: English-Spanish (Tony Lozano)",
rodex=u"ro: Romanian DEX",
webst_unab=u"en: Webster's Third New International",
dueae_vox=u"es: Uso del Español de América y España VOX".encode('us-ascii','xmlcharrefreplace'),
enes_adv_vox=u"es: Advanced English Spanish VOX",
esen_adv_vox=u"es: Advanced Spanish English VOX",
)


THE_DATABASE = u"dict"
START_CURRENT_DICT = u"collins"
db_file = u"sqlite3.db"
SPACE = u" "
DICT_PATH = 'dic'
IMG_DIR_MARK = '${imgdir}'
IMG_DIR = 'img'
##############################################################
##############################################################


def render(html):
	try:
		sys.stdout.write (html.encode('utf8'))
	except UnicodeDecodeError, err:
		sys.stdout.write (u"Charset error: " + str(err))


#def render_buf(buf):
	##return "".join(buf)
	#for line in buf:
		##sys.stderr.write (line)
		#try:
			#sys.stdout.write (line.encode('utf8'))
		#except UnicodeDecodeError, err:
			##print str(sys.stdout.encoding)
			#sys.stdout.write (u"Charset error: " + str(err))
			##raise UnicodeError("Unicode translation error: " + line)

sys.stderr = sys.stdout                # redirect errors to browser

form = cgi.FieldStorage( )            # parse form data
dikt = None

#cookie needs to be set before printing the 
#Content-Type line but after reading the form values
ck = Cookie.SimpleCookie()
if os.environ.has_key('HTTP_COOKIE'):
	ck.load(os.environ['HTTP_COOKIE'])
	if ck.has_key('dikt'):
		START_CURRENT_DICT = ck['dikt'].value
		
if form.has_key('dikt'):
	dikt = form['dikt'].value
	#ckn['dikt']['max-age'] = 1088640000 # 900 days
	#ckn['dikt']['path'] = '/'		
else:
	dikt = START_CURRENT_DICT
if dikt:
	sys.stdout.write( "Set-Cookie: dikt=" + dikt + '; expires=Sat, 23-Apr-2012 22:32:16 GMT; Version=1')      # plus blank line
	
sys.stdout.write('\n') # plus blank line	
sys.stdout.write( "Content-type: text/html\n")      # plus blank line


# if debug: print"DBG: ", cgi.print_form(form)

if form.has_key('word'):
	try:
		word= unicode( form['word'].value, "utf-8")
	except Exception, err:
		try:
			word= form['word'].value
		except UnicodeDecodeError, err:
			word = u"error"

else: # using "a" for debugging..
	word= ""
word = word.strip()

if form.has_key('next'):
	next= form['next'].value
else:
	next= ""


if form.has_key('hits'):
	hits= form['hits'].value
	#print form['hits'], "=", form['hits'].value, "<br>"
else:
	hits= 8

##if not form.has_key('dikt'):
	##dikt = START_CURRENT_DICT

if form.has_key('dikt'):
	dikt= form['dikt'].value
else:
	dikt= START_CURRENT_DICT

if form.has_key('where'):
	where= 'article'
else:
	where= ''

if hits:

	MAX_ROWS = int(hits)

else:
	MAX_ROWS = 16

HTML_HEAD = u"""
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 TRANSITIONAL//EN">
<html>
<head>

		  <title>Web Dictionary</title>
		  <meta http-equiv="content-type" content="text/html; charset=utf-8" />

<script src="/dict.js" type="text/javascript"></script>

<script language="JavaScript" type="text/javascript">

function changeDict(list) {
		  var newDict = list.options[list.selectedIndex].value;

		  location.href="?word="+document.forms[0].elements["word"].value+"&dikt="+ document.forms[0].elements["dikt"].value;
}

function inschar(c) {
    document.q.wrd.value += c;
    //document.q.wrd.focus();
}

</script>

<link REL="STYLESHEET" href="/dict.css" type="text/css">
</head>
<body bgcolor="#ffffff" text="#000000" link="#000099" vlink="#330066" alink="#ff0000" ondblclick=dictionary()>
<div align="center">
"""

#sys.stdout.write("DBGG:: type(word)" + str(type(word)))


render(HTML_HEAD)
# print HTML_HEAD

# print "<h1>%s</h1>" % dicts[dikt][4:]
#mystr.encode('us-ascii','xmlcharrefreplace')
render(u"<h1>")
render( dicts[dikt][4:])
render(u"</h1>")



render ( """
<table class="search" border="0" width="450">
<form method="get" action="" name="q" id="q">
<tr>
<td width='300'>Lookup:
<input type="text" name="word" id="wrd" onFocus="this.select()" value="%s">
<br>
in <INPUT class="nobrd" TYPE=RADIO NAME="where" VALUE="" %s > headword
<INPUT class="nobrd" TYPE=RADIO NAME="where" VALUE="body" %s > article
<br>
</td>
<td>

""" % ( word, (not where and 'CHECKED'), (where and 'CHECKED'))
)

###################################
########### MAX RESULTS
###################################
# generate max results sequence
F1,F2,F3,F4 = [2 ** i for i in range (3,7)]

render ( u''' Show first <select name="hits">
			 <option value="%s">%s</option>
<option value="%s">%s</option>
<option value="%s">%s</option>
<option value="%s">%s</option>
</td>
</tr>
<tr valign="middle">
<td>
''' % (F1,F1,F2,F2, F3,F3,F4,F4)
)


render ( u'<select name = "dikt" onchange="changeDict(this);">\n')
# sort dictionary by value (http://aspn.activestate.com/ASPN/Python/Cookbook/Recipe/52306)

for k,v in sorted(dicts.items(), key=lambda (k,v): (v,k)):
	render ( u'<option value="%s">%s</option>\n' % (k,v) )

# old, unsorted>> for k,v in dicts.iteritems():
#	print '<option value="%s">%s</option>\n' % (k,v)

# select current dictionary in listbox
render ( u"""
</select>
</td>
<td>
<input class="button" type="submit" value="Search">
<script language="JavaScript">
document.forms[0].elements["dikt"].value="%s"
</script>
</select>&nbsp;
</td>
</tr>
<tr>
<td>
<!--ôáíéýéñúţşîăâ-->

<a onclick="inschar('&#243;');" href="#" class="uml"> &#243; </a>
<a onclick="inschar('&#225;');" href="#" class="uml"> &#225; </a>
<a onclick="inschar('&#237;');" href="#" class="uml"> &#237; </a>
<a onclick="inschar('&#233;');" href="#" class="uml"> &#233; </a>
<a onclick="inschar('&#253;');" href="#" class="uml"> &#253; </a>
<a onclick="inschar('&#233;');" href="#" class="uml"> &#233; </a>
<a onclick="inschar('&#241;');" href="#" class="uml"> &#241; </a>
<a onclick="inschar('&#250;');" href="#" class="uml"> &#250; </a>
<a onclick="inschar('&#355;');" href="#" class="uml"> &#355; </a>
<a onclick="inschar('&#351;');" href="#" class="uml"> &#351; </a>
<a onclick="inschar('&#238;');" href="#" class="uml"> &#238; </a>
<a onclick="inschar('&#259;');" href="#" class="uml"> &#259; </a>
<a onclick="inschar('&#226;');" href="#" class="uml"> &#226; </a>

</td>
</tr>
</form>
</table>
""" % (dikt)
)


if word:
	render (u'''<script language="JavaScript">
document.forms[0].elements["hits"].value="%s";
document.forms[0].elements["dikt"].value="%s";
document.forms[0].elements["dikt"].focus();
</script>
''' % (hits, dikt)
)
else:
	render('''<script language="JavaScript">
			   document.forms[0].elements["word"].focus();
</script>
''')

# print the buffer here, so that if there is some error
# in the section below then there is at least something on the page

#sys.stdout.write( "".join (buf).encode('utf8', "replace"))
#render(buf)

###########################################################
##########################################################
############# DEBUG ONLY
###########################################################
##########################################################
#word = 'a'
#where = 'body'

if not word:
	pass

else:
	import re
	import sqlite3

	if where:

		RE_COLORIZER = re.compile(ur'(' + word + ur")(?=[^><]{,256}<)", re.IGNORECASE | re.UNICODE)

	if next:

		next = int(next)

	params = []
	##### !!!!! table names cannot be parametrized with '?'
	query = "SELECT * FROM %s WHERE"  % (dikt)

	got_one = False

	if not where:
		query += " headword LIKE ?"  ## word  //starts with `word`
		params += [word + '%']

	else:
		query += " definition LIKE ?" ## word  //starts with `word`
		params += ['%' + word + '%']
	#    query += " headword like '"+ word + "%'"

	if next:
		query += " LIMIT ? OFFSET ?" ##, [str(MAX_ROWS),  str(next)]
		params += [str(MAX_ROWS),  str(next)]
#		query += " LIMIT "+ str(MAX_ROWS)  +" OFFSET "+  str(next)

	#render ( '<pre class="dbg">DBG: query=%s</pre>' % query)

	cn = sqlite3.connect(DICT_PATH + os.path.sep + dikt + '.db')
	cursor = cn.cursor()

	try:
		cursor.execute(query, params)
	except Exception, err:
		sys.stdout.write('<p class="error">\nSorry, ' + str(err) + '</p>')
		sys.stdout.write("\n</div></body></html>")
		sys.exit(-2)

	#rows = cursor.fetchmany(MAX_ROWS)
	rows = cursor.fetchmany(10)
	current_row_count = 0

	if not rows:
		sys.stdout.write('\n<p class="error">No results found.</p>')
	else:
		next_row_count    = 0
		previous_row_count = 0
		current_row_count = len(rows)

	if not next:
		next =0
	next_row_count = next + current_row_count

	previous_row_count = next - MAX_ROWS

	PREVIOUS_LINK =  (previous_row_count > -1) and \
				  u'<a href="?word=%s&next=%s&hits=%s&dikt=%s&where=%s" title="Previous Entries"> &laquo; Previous </a></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' \
				  %  (word, previous_row_count, MAX_ROWS, dikt, where) or ""
	#
	NEXT_LINK = (current_row_count >= MAX_ROWS) and \
			  u'<a href="?word=%s&next=%s&hits=%s&dikt=%s&where=%s" title="Next Entries"> Next &raquo;</a>&nbsp;' \
			  % (word, next_row_count, MAX_ROWS, dikt, where ) or ""
	#
	render( PREVIOUS_LINK)

	render( NEXT_LINK )
	render (u"  <dl>")

	for row in rows:
		render(u"  \n<dt>" +  unicode(row[0])  + u"</dt>")

		## convert 'class="ref" to links
		if u'class="ref"' in row[1]:
			if where:
				row = re.sub(RE_COLORIZER, u'<span class="h">' + u'\\1' + u'</span>', row[1])
			else:
				row = row[1]
			context = u"&dikt=%s&hits=%s" % (dikt, str(MAX_ROWS))
			row = re.sub(u'(<span class="ref">)([^<]+)(</span>)',
							 u'\\1<a href="?word=\\2' + context + '">\\2</a>\\3' , row)
		else:
			if where:
				row = re.sub(RE_COLORIZER, u'<span class="h">' + u'\\1' + u'</span>', row[1])

			else:
				row = row[1]

		if IMG_DIR_MARK in row:
			row = row.replace(IMG_DIR_MARK, '../' + DICT_PATH + '/' + dikt + '/')
		render (u"\n <dd>")

		render(row)

		#print "  \n<dd>" + str( row[1].replace('\n','\n</dd>\n<dd>') )+ "</dd>"
		#
	render( u""" </dl>
				</table>"""
)
	render(PREVIOUS_LINK)
	render(NEXT_LINK)
#
render( u"""
</div>
</body>
</html>

"""
)

#render(buf)
