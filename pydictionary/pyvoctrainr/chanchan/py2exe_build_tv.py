# setup.py
from distutils.core import setup
import py2exe
#setup(window=["pyvoctr.py"])

setup(
	windows = [
		{
			"script" : "chanchan.py",
			"icon_resources" : [(0, "chanchan.ico")]			
		}
	], 
	options = 
		{
			"py2exe" : {"includes" : ["sip", "PyQt4"]}
		}
)
