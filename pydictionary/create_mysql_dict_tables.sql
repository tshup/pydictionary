
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `dict`;

USE `dict`;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `americana` */

DROP TABLE IF EXISTS `americana`;

CREATE TABLE `americana` (
  `headword` varchar(256) character set utf8 collate utf8_unicode_ci NOT NULL,
  `definition` mediumtext character set utf8 collate utf8_unicode_ci,
  PRIMARY KEY  (`headword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `cambridge` */

DROP TABLE IF EXISTS `cambridge`;

CREATE TABLE `cambridge` (
  `headword` varchar(256) NOT NULL,
  `definition` text,
  PRIMARY KEY  (`headword`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `collins` */

DROP TABLE IF EXISTS `collins`;

CREATE TABLE `collins` (
  `headword` varchar(256) character set utf8 collate utf8_unicode_ci NOT NULL,
  `definition` mediumtext character set utf8 collate utf8_unicode_ci,
  PRIMARY KEY  (`headword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `longman` */

DROP TABLE IF EXISTS `longman`;

CREATE TABLE `longman` (
  `headword` varchar(256) character set utf8 collate utf8_unicode_ci NOT NULL,
  `definition` text character set utf8 collate utf8_unicode_ci,
  PRIMARY KEY  (`headword`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `oxford` */

DROP TABLE IF EXISTS `oxford`;

CREATE TABLE `oxford` (
  `headword` varchar(256) character set utf8 collate utf8_unicode_ci NOT NULL,
  `definition` mediumtext character set utf8 collate utf8_unicode_ci,
  PRIMARY KEY  (`headword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `webster`;

CREATE TABLE `webster` (
  `headword` varchar(256) character set utf8 collate utf8_unicode_ci NOT NULL,
  `definition` mediumtext character set utf8 collate utf8_unicode_ci,
  PRIMARY KEY  (`headword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;